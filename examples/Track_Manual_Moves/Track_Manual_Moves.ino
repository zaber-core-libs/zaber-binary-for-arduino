#include <ZaberBinary.h>
 
ZaberShield shield(ZABERSHIELD_ADDRESS_AA);
ZaberBinary zb(shield);
 
void setup() {
    shield.begin(9600);

    // This example uses the serial monitor for output and a Zaber shield 
    // for device interfacing at the same time. This conbination makes
    // debugging easier.
    Serial.begin(9600);
    Serial.println("Starting...");
}
 
void loop() {

  // Broadcast a "return current position" command to all devices 
  // that are connected to the shield.
  zb.send(0, ZaberBinary::Command::GET_POS, 0);

  // Read responses until there are none left.
  while (zb.isReplyAvailable())
  {
    ZaberBinary::reply reply = zb.receive();
    if (reply.isError)
    {
      Serial.println("Got an error from device " + String(reply.deviceNumber) + ": " + String(reply.responseData));
    }
    else if (reply.commandNumber == 8)
    {
      Serial.println("Auto move tracking from device " + String(reply.deviceNumber) + ": Position = " + String(reply.responseData));
    }
    else if (reply.commandNumber == 10)
    {
      Serial.println("Manual move tracking from device " + String(reply.deviceNumber) + ": Position = " + String(reply.responseData));
    }
    else if (reply.commandNumber == ZaberBinary::Command::GET_POS)
    {
      Serial.println("Device " + String(reply.deviceNumber) + " position = " + String(reply.responseData));
    }
    else
    {
      Serial.println("Got an unexpected message from device " + String(reply.deviceNumber) + ": " + String(reply.commandNumber));
    }
  }

  // We've now processed all received messages and can do
  // some other work until the next pass throug the loop.
  delay(100);
}
